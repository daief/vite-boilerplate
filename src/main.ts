import { createApp } from 'vue';
import './index.less';
import App from './app.vue';
import { router } from './routes';

const app = createApp(App);

app.use(router);

router.isReady().then(() => {
  app.mount('#app');
});
