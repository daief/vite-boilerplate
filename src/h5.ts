const base = 375;
const t = document;
let metaViewport = t.querySelector<HTMLMetaElement>('meta[name="viewport"]');
if (!metaViewport) {
  metaViewport = t.createElement('meta');
  metaViewport.setAttribute('name', 'viewport');
  t.head.appendChild(metaViewport);
}

const androidVersion = Number(
  /Android (\d+\.\d+)/i.exec(navigator.userAgent)?.[1] || 0,
);

// Check if the device is Android
if (androidVersion) {
  const rate = parseInt(String(window.screen.width)) / base;
  if (androidVersion > 2.3) {
    // Check if the device is a Motorola phone
    if (/MZ-M571C/.test(navigator.userAgent)) {
      // Set the meta viewport for a Motorola phone
      metaViewport.setAttribute(
        'content',
        `width=${base}, minimum-scale = 0.5, maximum-scale= 0.5, viewport-fit=cover`,
      );
    } else {
      // Set the meta viewport for other Android phones
      metaViewport.setAttribute(
        'content',
        `width=${base}, viewport-fit=cover, minimum-scale = ` +
          rate +
          ', maximum-scale = ' +
          rate +
          ', target-densitydpi=device-dpi',
      );
    }
  } else {
    // Set the meta viewport for other devices
    metaViewport.setAttribute(
      'content',
      `width=${base}, target-densitydpi=device-dpi, viewport-fit=cover`,
    );
  }
} else {
  metaViewport.setAttribute(
    'content',
    `width=${base}, user-scalable=no, target-densitydpi=device-dpi, viewport-fit=cover`,
  );
}

export {};
